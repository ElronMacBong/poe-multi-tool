﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.Extensions.Logging;

namespace PMT.Mediator.Internal {
    internal class LoggingMediator : MediatR.Mediator {
        private readonly ILogger _logger;

        public LoggingMediator(
            ILogger logger,
            ServiceFactory serviceFactory) : base(serviceFactory) {
            _logger = logger;
        }

        protected override async Task PublishCore(IEnumerable<Func<INotification, CancellationToken, Task>> allHandlers, INotification notification, CancellationToken cancellationToken) {
            foreach (Func<INotification, CancellationToken, Task> handler in allHandlers) {
                Type handlerType = handler?.Target?.GetType().GetFields()?.FirstOrDefault()?.GetValue(handler.Target)?.GetType();
                string handlerNameWithGuid = $"[{Guid.NewGuid().ToString().Substring(0, 8)}] { handlerType?.FullName ?? notification?.GetType()?.FullName}";
                Stopwatch stopwatch = Stopwatch.StartNew();

                _logger.LogInformation($"[Mediator:START] {handlerNameWithGuid}");

                await handler(notification, cancellationToken).ConfigureAwait(false);

                stopwatch.Stop();
                _logger.LogInformation($"[Mediator:END]   {handlerNameWithGuid}; Execution time={stopwatch.ElapsedMilliseconds}ms");
            }
        }
    }
}
