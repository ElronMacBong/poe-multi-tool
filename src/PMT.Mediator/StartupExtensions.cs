﻿using System;
using System.Reflection;

using MediatR;

using Microsoft.Extensions.DependencyInjection;

namespace PMT.Mediator {
    public static class StartupExtensions {
        public static IServiceCollection AddMediator(this IServiceCollection services, params Assembly[] assemblies) {
            services
                .AddMediatR((config) => config.Using<Mediator>().AsSingleton(), assemblies);

            services
                .AddSingleton<IMediatorTask, Mediator>((sp) => (Mediator)sp.GetService<IMediator>())
                .AddSingleton((sp) => (Mediator)sp.GetService<IMediator>());

            return services;
        }
    }
}
