﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.Extensions.Logging;

using PMT.Mediator.Internal;

namespace PMT.Mediator {
    public class Mediator : IMediatorTask, IMediator {
        private readonly ILogger _logger;
        private readonly LoggingMediator _mediator;

        private IList<Task> _runningTasks { get; set; } = new List<Task>();



        public Mediator(
            ILogger<Mediator> logger,
            ServiceFactory serviceFactory) {
            _logger = logger;
            _mediator = new LoggingMediator(logger, serviceFactory);
        }



        private Task AddTask(Task task) {
            AddRunningTask(task);

            return task;
        }
        private Task<T> AddTask<T>(Task<T> task) {
            AddRunningTask(task);

            return task;
        }
        private void AddRunningTask(Task task) {
            lock (_runningTasks) {
                _runningTasks = _runningTasks.Where(x => !x.IsCanceled && !x.IsFaulted && !x.IsCompleted).ToList();
                _runningTasks.Add(task);
            }
        }



        public Task<Unit> Command(ICommand command, CancellationToken cancellationToken = default) {
            return AddTask(_mediator.Send(command, cancellationToken));
        }

        public Task<TResponse> Command<TResponse>(ICommand<TResponse> command, CancellationToken cancellationToken = default) {
            return AddTask(_mediator.Send(command, cancellationToken));
        }

        public Task<TResponse> Query<TResponse>(IQuery<TResponse> query, CancellationToken cancellationToken = default) {
            return AddTask(Send(query, cancellationToken));
        }

        public Task Notify<TNotification>(TNotification notification, CancellationToken cancellationToken = default) where TNotification : INotification {
            return Publish(notification, cancellationToken);
        }

        public Task WhenAll => Task.Run(async () => await Task.WhenAll(_runningTasks.Select(x => x).ToList()));



        public Task<TResponse> Send<TResponse>(IRequest<TResponse> request, CancellationToken cancellationToken = default) {
            return AddTask(_mediator.Send(request, cancellationToken));
        }

        public Task<object> Send(object request, CancellationToken cancellationToken = default) {
            return AddTask(_mediator.Send(request, cancellationToken));
        }

        public async Task Publish(object notification, CancellationToken cancellationToken = default) {
            string guid = $"[{Guid.NewGuid().ToString().Substring(0, 8)}]";
            string nameWithGuid = $"{guid} { notification?.GetType()?.FullName}";
            Stopwatch stopwatch = Stopwatch.StartNew();

            try {
                _logger.LogInformation($"[Mediator:NOTIF] {nameWithGuid}");

                try {
                    string props = JsonSerializer.Serialize(notification);
                    if (props != "{}")
                        _logger.LogInformation($"[Mediator:PROPS] {guid} {props}");
                } catch (Exception) {
                    _logger.LogError($"[Mediator:ERROR] {guid} Could not serialize the notification.");
                }

                await AddTask(_mediator.Publish(notification, cancellationToken));
            } catch (Exception e) {
                _logger.LogError($"[Mediator:ERROR] {nameWithGuid} - {e.Message}");

                throw;
            } finally {
                stopwatch.Stop();
                _logger.LogInformation($"[Mediator:END]   {nameWithGuid}; Execution time={stopwatch.ElapsedMilliseconds}ms");
            }
        }
        public Task Publish<TNotification>(TNotification notification, CancellationToken cancellationToken = default) where TNotification : INotification {
            return Publish((object)notification, cancellationToken);
        }
    }
}
