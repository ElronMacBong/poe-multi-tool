﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

namespace PMT.Mediator {
    public interface IMediatorTask {
        Task<Unit> Command(ICommand command, CancellationToken cancellationToken = default);
        Task<TResponse> Command<TResponse>(ICommand<TResponse> command, CancellationToken cancellationToken = default);

        Task<TResponse> Query<TResponse>(IQuery<TResponse> query, CancellationToken cancellationToken = default);

        Task Notify<TNotification>(TNotification notification, CancellationToken cancellationToken = default) where TNotification : INotification;
    }
}
