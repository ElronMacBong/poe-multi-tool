﻿using System;
using System.Collections.Generic;
using System.Text;

using MediatR;

namespace PMT.Domain.Notification.Commands {
    /// <summary>
    /// Shows a notification message.
    /// </summary>
    public class NotificationCommand : ICommand {
        /// <summary>
        /// The title of the notification (optional).
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// The message in the notification.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Shows a notification message.
        /// </summary>
        /// <param name="message">The message in the notification.</param>
        public NotificationCommand(string message) { Message = message; }
        /// <summary>
        /// Shows a notification message.
        /// </summary>
        /// <param name="message">The message in the notification.</param>
        /// <param name="title">The title of the notification.</param>
        public NotificationCommand(string message, string title) { Message = message; Title = title; }
    }
}
