﻿namespace PMT.Domain.MessageBox.Commands {
    public enum MessageBoxOptions {
        None = 0,
        DefaultDesktopOnly = 131072,
        RightAlign = 524288,
        RtlReading = 1048576,
        ServiceNotification = 2097152
    }
}