﻿using System;
using System.Collections.Generic;
using System.Text;

using MediatR;

namespace PMT.Domain.MessageBox.Commands {
    /// <summary>
    /// Command to show a message box in WPF.
    /// </summary>
    public class MessageBoxCommand : ICommand {
        /// <summary>
        /// Holds the text of the message box.
        /// </summary>
        public string Text { get; }
        /// <summary>
        /// Holds the caption of the message box.
        /// </summary>
        public string Caption { get; }
        /// <summary>
        /// The buttons of the message box.
        /// </summary>
        public MessageBoxButton Button { get; }
        /// <summary>
        /// The icon of the message box.
        /// </summary>
        public MessageBoxImage Icon { get; }
        /// <summary>
        /// The result of the message box.
        /// </summary>
        public MessageBoxResult Result { get; }
        /// <summary>
        /// The options of the message box.
        /// </summary>
        public MessageBoxOptions Options { get; }

        /// <summary>Command to show a message box.</summary>
        /// <param name="text">The text of the message box.</param>
        public MessageBoxCommand(string text) { Text = text; }
        /// <summary>Command to show a message box.</summary>
        /// <param name="text">The text of the message box.</param>
        /// <param name="caption">The caption of the message box.</param>
        public MessageBoxCommand(string text, string caption) { Text = text; Caption = caption; }
        /// <summary>Command to show a message box.</summary>
        /// <param name="text">The text of the message box.</param>
        /// <param name="caption">The caption of the message box.</param>
        /// <param name="button">The button of the message box.</param>
        public MessageBoxCommand(string text, string caption, MessageBoxButton button) { Text = text; Caption = caption; Button = button; }
        /// <summary>Command to show a message box.</summary>
        /// <param name="text">The text of the message box.</param>
        /// <param name="caption">The caption of the message box.</param>
        /// <param name="button">The button of the message box.</param>
        /// <param name="icon">The icon of the message box.</param>
        public MessageBoxCommand(string text, string caption, MessageBoxButton button, MessageBoxImage icon) { Text = text; Caption = caption; Button = button; Icon = icon; }
        /// <summary>Command to show a message box.</summary>
        /// <param name="text">The text of the message box.</param>
        /// <param name="caption">The caption of the message box.</param>
        /// <param name="button">The button of the message box.</param>
        /// <param name="icon">The icon of the message box.</param>
        /// <param name="result">The result of the message box.</param>
        public MessageBoxCommand(string text, string caption, MessageBoxButton button, MessageBoxImage icon, MessageBoxResult result) { Text = text; Caption = caption; Button = button; Icon = icon; Result = result; }
        /// <summary>Command to show a message box.</summary>
        /// <param name="text">The text of the message box.</param>
        /// <param name="caption">The caption of the message box.</param>
        /// <param name="button">The button of the message box.</param>
        /// <param name="icon">The icon of the message box.</param>
        /// <param name="result">The result of the message box.</param>
        /// <param name="options">The options of the message box.</param>
        public MessageBoxCommand(string text, string caption, MessageBoxButton button, MessageBoxImage icon, MessageBoxResult result, MessageBoxOptions options) { Text = text; Caption = caption; Button = button; Icon = icon; Result = result; Options = options; }
    }
}
