﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PMT.Domain.MessageBox.Commands {
    public enum MessageBoxButton {
        OK = 0,
        OKCancel = 1,
        YesNoCancel = 3,
        YesNo = 4
    }
}
