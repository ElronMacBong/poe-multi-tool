﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

using PMT.Domain.Settings;

namespace PMT.Domain.ViewModels {
    public interface ISettingsViewModel {
        ICommand AddNewChatCommand { get; set; }

        void OnAddNewCommandClicked(object obj);
    }
}
