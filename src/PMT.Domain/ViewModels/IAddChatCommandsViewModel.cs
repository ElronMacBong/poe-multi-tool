﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace PMT.Domain.ViewModels {
    public interface IAddChatCommandsViewModel {
        ICommand AddChatCommand { get; set; }

        void OnAddChatCommandClicked(object obj);
    }
}
