﻿namespace PMT.Domain.ViewModels {
    public interface IInitializationViewModel {
        int Progress { get; set; }
    }
}
