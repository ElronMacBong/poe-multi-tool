﻿using System;
using System.Collections.Generic;
using System.Text;

using PMT.Domain.Game.ChatCommand;

namespace PMT.Domain.ViewModels {
    public interface IChatCommandViewModel {
        IChatCommand Command { get; set; }
    }
}
