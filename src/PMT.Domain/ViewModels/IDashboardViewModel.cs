﻿using System.Windows.Input;

namespace PMT.Presentation.WPF.ViewModels {
    public interface IDashboardViewModel {
        public ICommand QuitCommand { get; set; }
        public ICommand SettingsCommand { get; set; }
        public ICommand ChangelogCommand { get; set; }
        public ICommand AboutCommand { get; set; }

        void OnQuitClicked(object obj);
        void OnSettingsClicked(object obj);
        void OnChangelogClicked(object obj);
        void OnAboutClicked(object obj);
    }
}
