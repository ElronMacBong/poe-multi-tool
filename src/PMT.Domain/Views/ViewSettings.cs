﻿namespace PMT.Domain.Views {
    public class ViewSettings : IViewSettings {
        public string Title { get; set; } = "PoE Multi-Tool";

        public bool IsModal { get; set; }
        public bool IsOverlay { get; set; }
        public bool ShowInTaskbar { get; set; }

        public int Height { get; set; }
        public int Width { get; set; }
    }
}
