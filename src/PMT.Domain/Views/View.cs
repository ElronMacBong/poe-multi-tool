﻿namespace PMT.Domain.Views {
    public enum View {
        Initialization,
        Setup,
        Dashboard,
        Settings,
        MainOverlay,
        WebBrowser,

        AddChatCommand
    }
}
