﻿namespace PMT.Domain.Views {
    public interface IViewSettings {
        string Title { get; set; }

        bool IsModal { get; set; }
        bool IsOverlay { get; set; }
        bool ShowInTaskbar { get; set; }

        int Height { get; set; }
        int Width { get; set; }
    }
}
