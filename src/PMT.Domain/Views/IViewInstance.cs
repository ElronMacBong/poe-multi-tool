﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PMT.Domain.Views {
    public interface IViewInstance {
        public View View { get; }

        public ViewSettings Settings { get; }

        Task Close();

        Task Minimize();
        Task Maximize();
    }
}
