﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PMT.Domain.Views {
    public interface IViewHandler {
        Task Open(View view, params object[] args);

        void Close(View view);
        void CloseAll();

        bool IsOpened(View view);
        bool IsAnyOpened();
    }
}
