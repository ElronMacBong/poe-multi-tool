﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PMT.Domain.Platforms {
    public interface IKeyboardProvider {
        event Action<string> OnKeyDown;
        event Action<string> OnKeyUp;

        void Initialize();

        void PressKey(params string[] keys);

        bool IncludesModifier(string input);
    }
}
