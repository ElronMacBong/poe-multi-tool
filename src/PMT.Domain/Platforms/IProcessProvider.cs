﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace PMT.Domain.Platforms {
    public interface IProcessProvider {
        event Action OnFocus;
        event Action OnBlur;

        bool IsPathOfExileInFocus { get; }
        bool IsPoeMultiToolInFocus { get; }

        Process GetPathOfExileProcess();
        public Task Initialize(CancellationToken cancellationToken);

        /*
        event Action OnFocus;
        event Action OnBlur;

        string ClientLogPath { get; }

        bool IsPathOfExileInFocus { get; }
        bool IsPMTInFocus { get; }

        Task Initialize(CancellationToken cancellationToken);

        Task DisposeAsync();
        */
    }
}
