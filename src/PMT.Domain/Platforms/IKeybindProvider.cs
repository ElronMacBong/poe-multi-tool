﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PMT.Domain.Platforms {
    public interface IKeybindProvider {
        void Initialize();

        void Register();
        void Unregister();

        void AddChatCommand(string key, string command, bool send, bool enabled);
        void RemoveChatCommand(string key);
    }
}
