﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PMT.Domain.Platforms {
    public interface IKeybindHandler {
        public void Execute();
    }
}
