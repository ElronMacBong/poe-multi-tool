﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Extensions.Logging;

using PMT.Domain.Platforms;

namespace PMT.Domain.App.Commands {
    public class ChatCommandHideoutCommand : IKeybindHandler {
        private readonly ILogger _logger;
        private readonly IKeyboardProvider _keyboardProvider;
        private readonly IClipboardProvider _clipboardProvider;

        public ChatCommandHideoutCommand(
            ILogger<ChatCommandHideoutCommand> logger,
            IKeyboardProvider keyboardProvider,
            IClipboardProvider clipboardProvider) {
            _logger = logger;
            _keyboardProvider = keyboardProvider;
            _clipboardProvider = clipboardProvider;
        }

        public void Execute() {
            _clipboardProvider.SetText("/hideout");
            _keyboardProvider.PressKey("Enter", "Paste", "Enter");
        }
    }
}
