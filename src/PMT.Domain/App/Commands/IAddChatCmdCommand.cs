﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using MediatR;

namespace PMT.Domain.App.Commands {
    public interface IAddChatCmdCommand : ICommand {
        string Key { get; }
        string Command { get; }
        bool Send { get; }
    }
}
