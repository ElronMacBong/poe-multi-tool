﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PMT.Domain.App.Commands {
    public class AddChatCmdCommand : IAddChatCmdCommand {
        public string Key { get; }
        public string Command { get; }
        public bool Send { get; }

        public AddChatCmdCommand(string key, string command, bool send) {
            Key = key;
            Command = command;
            Send = send;
        }
    }
}
