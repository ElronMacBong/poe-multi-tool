﻿using System;
using System.Collections.Generic;
using System.Text;

using MediatR;

namespace PMT.Domain.App.Commands {
    /// <summary>
    /// Shutdown the application.
    /// </summary>
    public class ShutdownCommand : ICommand { }
}
