﻿namespace PMT.Domain.Settings {
    public interface IGeneralSettings {
        string LanguageCode { get; set; }

        bool CloseToTray { get; set; }
    }
}
