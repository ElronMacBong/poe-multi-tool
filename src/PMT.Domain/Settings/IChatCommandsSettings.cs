﻿using System;
using System.Collections.Generic;
using System.Text;

using PMT.Domain.Game.ChatCommand;

namespace PMT.Domain.Settings {
    public interface IChatCommandsSettings {
        List<ChatCommand> Commands { get; set; }
    }
}
