﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PMT.Domain.Settings {
    public interface ISettingsReader {
        string ConfigurationFilePath { get; set; }

        T Load<T>() where T : class, new();
        object Load(Type type);

        T LoadSection<T>() where T : class, new();
        object LoadSection(Type type);
    }
}
