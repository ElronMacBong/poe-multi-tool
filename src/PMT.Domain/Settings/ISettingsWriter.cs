﻿using System;
using System.Threading.Tasks;

namespace PMT.Domain.Settings {
    public interface ISettingsWriter {
        string ConfigurationFilePath { get; set; }

        Task WriteAsync<T>(object config);
        Task WriteAsync(object config, Type type);
    }
}
