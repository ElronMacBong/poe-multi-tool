﻿using System;
using System.Collections.Generic;
using System.Text;

using PMT.Domain.Game.ChatCommand;

namespace PMT.Domain.Settings {
    public interface IAppSettings {
        IGeneralSettings General { get; set; }
        IPathOfExileSettings PathOfExile { get; set; }
        IOverlaySettings Overlay { get; set; }
        IChatCommandsSettings ChatCommands { get; set; }
        IGuideSettings Guide { get; set; }
        ITradeSettings Trade { get; set; }
    }
}
