﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PMT.Domain.Settings {
    public interface IPathOfExileSettings {
        string SessionId { get; set; }

        string LeagueId { get; set; }
        string LeagueHash { get; set; }
    }
}
