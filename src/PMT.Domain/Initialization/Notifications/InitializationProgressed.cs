﻿using System;
using System.Collections.Generic;
using System.Text;

using MediatR;

namespace PMT.Domain.Initialization.Notifications {
    public class InitializationProgressed : INotification {
        public string Title { get; set; }
        public int Percentage { get; set; }

        public InitializationProgressed(int percentage) {
            Percentage = percentage;
        }
    }
}
