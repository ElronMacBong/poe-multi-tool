﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

using PMT.Domain.Keybinds;

namespace PMT.Domain.Game.ChatCommand {
    public interface IChatCommand {
        string Key { get; set; }
        string Command { get; set; }
        bool Send { get; set; }
        bool Enabled { get; set; }

        void DeleteChatCommand();
    }
}
