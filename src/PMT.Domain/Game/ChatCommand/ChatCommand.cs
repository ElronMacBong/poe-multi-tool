﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

using Microsoft.Extensions.Logging;

using PMT.Domain.Platforms;

namespace PMT.Domain.Game.ChatCommand {
    public class ChatCommand : IChatCommand {
        private ILogger _logger;
        private IKeybindProvider _keybindProvider;

        public string Key { get; set; }
        public string Command { get; set; }
        public bool Send { get; set; }
        public bool Enabled { get; set; }

        public ChatCommand(
            IKeybindProvider keybindProvider) {
            _keybindProvider = keybindProvider;
        }

        public void DeleteChatCommand() {
            _keybindProvider.RemoveChatCommand(Key);
        }
    }
}
