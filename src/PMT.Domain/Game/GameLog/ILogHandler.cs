﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PMT.Domain.Game.GameLog {
    public interface ILogHandler {
        void GetLastEntry();
    }
}
