﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PMT.Domain.Game.Languages {
    public interface IGameLanguage {
        string LanguageCode { get; }

        Uri PoeApiBaseUrl { get; }
    }
}
