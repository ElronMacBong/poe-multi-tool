﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PMT.Domain.Game.Languages {
    public interface IGameLanguageProvider {
        IGameLanguage Language { get; set; }
    }
}
