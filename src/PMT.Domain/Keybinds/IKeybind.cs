﻿using PMT.Domain.Game.ChatCommand;
using PMT.Domain.Platforms;

namespace PMT.Domain.Keybinds {
    public interface IKeybind {
        IChatCommand Command { get; set; }

        void Execute();
    }
}