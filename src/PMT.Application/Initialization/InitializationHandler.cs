﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using PMT.Application.Settings;
using PMT.Domain.Game.ChatCommand;
using PMT.Domain.Initialization.Commands;
using PMT.Domain.Initialization.Notifications;
using PMT.Domain.MessageBox.Commands;
using PMT.Domain.Platforms;
using PMT.Domain.Settings;
using PMT.Domain.Views;

namespace PMT.Application.Initialization {
    public class InitializationHandler : ICommandHandler<InitializeCommand> {
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly IViewHandler _viewHandler;
        private readonly IProcessProvider _processProvider;
        private readonly IKeyboardProvider _keyboardProvider;
        private readonly IKeybindProvider _keybindProvider;
        private readonly IAppSettings _settings;

        private int _count;
        private int _completed;

        public InitializationHandler(
            ILogger<InitializationHandler> logger,
            IMediator mediator,
            IViewHandler viewHandler,
            IProcessProvider processProvider,
            IKeyboardProvider keyboardProvider,
            IKeybindProvider keybindProvider,
            IAppSettings settings) {
            _logger = logger;
            _mediator = mediator;
            _viewHandler = viewHandler;
            _processProvider = processProvider;
            _keyboardProvider = keyboardProvider;
            _keybindProvider = keybindProvider;
            _settings = settings;
        }

        public async Task<Unit> Handle(InitializeCommand request, CancellationToken cancellationToken) {
            Stopwatch stopwatch = new Stopwatch();

            try {
                stopwatch.Start();

                _completed = 0;
                _count = request.FirstRun ? 10 : 7;
                await ReportProgress();

                _viewHandler.CloseAll();
                await _viewHandler.Open(View.Initialization);

                if (request.FirstRun) {

                }

                if (request.FirstRun) {
                    await RunAsync(() => _processProvider.Initialize(cancellationToken));
                    await RunAsync(() => _keyboardProvider.Initialize());
                    await RunAsync(() => _keybindProvider.Initialize());
                }

                await AddDefaultChatCommands();

                _completed = _count;
                stopwatch.Stop();
                _logger.LogInformation($"Initialized in {stopwatch.ElapsedMilliseconds}ms");
                await ReportProgress();
                await Task.Delay(500);

                _viewHandler.Close(View.Initialization);
                await _viewHandler.Open(View.Dashboard);

                return Unit.Value;
            } catch (Exception ex) {
                await _mediator.Send(new MessageBoxCommand(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error));

                return Unit.Value;
            }
        }



        private Task AddDefaultChatCommands() {
            _logger.LogDebug($"Count: {_settings.ChatCommands.Commands.Count}");

            if (_settings.ChatCommands.Commands.Count > 0)
                return Task.CompletedTask;

            AddChatCommand("F1", "/remaining", true, true);
            AddChatCommand("F2", "/exit", true, true);
            AddChatCommand("F3", "/dnd", true, true);
            AddChatCommand("F4", "/kick", true, true);
            AddChatCommand("F5", "/hideout", true, true);
            //AddChatCommand("Ctrl+Enter", "@{LastWhisper.CharacterName} ", false, false);

            _logger.LogDebug($"Count: {_settings.ChatCommands.Commands.Count}");
            return Task.CompletedTask;
        }

        private Task AddChatCommand(string key, string command, bool send, bool enabled) {
            ChatCommand cmd = new ChatCommand(_keybindProvider);
            cmd.Key = key;
            cmd.Command = command;
            cmd.Send = true;
            cmd.Enabled = enabled;

            _settings.ChatCommands.Commands.Add(cmd);

            return Task.CompletedTask;
        }



        private async Task RunAsync(Func<Task> func) {
            await func.Invoke();

            _completed += 1;

            await ReportProgress();
        }
        private async Task RunAsync(Action action) {
            action.Invoke();

            _completed += 1;

            await ReportProgress();
        }

        private async Task RunCommandStepAsync<TCommand>(TCommand command, bool shouldRun = true) where TCommand : ICommand {
            if (!shouldRun)
                return;

            await _mediator.Send(command);

            _completed += 1;

            await ReportProgress();
        }

        private async Task ReportProgress() {
            InitializationProgressed args = new InitializationProgressed(_count == 0 ? 0 : _completed * 100 / _count);
            if (args.Percentage >= 100) {
                args.Title = "Ready";
                args.Percentage = 100;
            } else {
                args.Title = $"{_completed}/{_count}";
            }

            await _mediator.Publish(args);
        }
    }
}
