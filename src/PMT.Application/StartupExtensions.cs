﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using PMT.Application.Settings;
using PMT.Domain.App.Commands;
using PMT.Domain.Game.ChatCommand;
using PMT.Domain.Settings;

namespace PMT.Application {
    public static class StartupExtensions {
        public static IServiceCollection AddApplication(this IServiceCollection services, IConfiguration configuration) {
            services
                .AddSingleton<ISettingsReader, SettingsReader>()
                .AddSingleton<ISettingsWriter, SettingsWriter>()
                .AddSingleton<IAppSettings, AppSettings>();

            services
                .AddTransient<IChatCommand, ChatCommand>();

            return services;
        }
    }
}
