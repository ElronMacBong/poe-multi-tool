﻿using System;
using System.Collections.Generic;
using System.Text;

using PMT.Domain.Settings;

namespace PMT.Application.Settings {
    public class PathOfExileSettings : IPathOfExileSettings {
        public string SessionId { get; set; } = "";

        public string LeagueId { get; set; } = "";
        public string LeagueHash { get; set; } = "";
    }
}
