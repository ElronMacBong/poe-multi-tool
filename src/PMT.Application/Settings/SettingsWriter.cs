﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

using Newtonsoft.Json;

using PMT.Domain.Game.ChatCommand;
using PMT.Domain.Settings;

namespace PMT.Application.Settings {
    public class SettingsWriter : ISettingsWriter {
        public string ConfigurationFilePath { get; set; }

        private ILogger _logger;

        private static readonly JsonSerializerSettings JsonSerializerSettings = new JsonSerializerSettings() {
            ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        };

        public SettingsWriter(
            ILogger<SettingsWriter> logger) {
            _logger = logger;
        }

        public async Task WriteAsync<T>(object config) {
            await WriteAsync(config, typeof(T));
        }
        public async Task WriteAsync(object config, Type type) {
            string json = JsonConvert.SerializeObject(config, type, JsonSerializerSettings);
            _logger.LogDebug($"JSON settings: {json}");

            await File.WriteAllTextAsync(ConfigurationFilePath, json);

            await Task.CompletedTask;
        }
    }
}
