﻿using System;
using System.Collections.Generic;
using System.Text;

using PMT.Domain.Game.ChatCommand;
using PMT.Domain.Settings;

namespace PMT.Application.Settings {
    public class ChatCommandsSettings : IChatCommandsSettings {
        public List<ChatCommand> Commands { get; set; } = new List<ChatCommand>();
    }
}
