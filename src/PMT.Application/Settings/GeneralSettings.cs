﻿using System;
using System.Collections.Generic;
using System.Text;

using PMT.Domain.Settings;

namespace PMT.Application.Settings {
    public class GeneralSettings : IGeneralSettings {
        public string LanguageCode { get; set; }

        public bool CloseToTray { get; set; }
    }
}
