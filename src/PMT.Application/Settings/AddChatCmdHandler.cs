﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using PMT.Domain.App.Commands;
using PMT.Domain.Game.ChatCommand;
using PMT.Domain.Platforms;
using PMT.Domain.Settings;

namespace PMT.Application.Settings {
    public class AddChatCmdHandler : ICommandHandler<AddChatCmdCommand> {
        private readonly IAppSettings _appSettings;
        private readonly IKeybindProvider _keybindProvider;

        public AddChatCmdHandler(
            IAppSettings appSettings,
            IKeybindProvider keybindProvider) {
            _appSettings = appSettings;
            _keybindProvider = keybindProvider;
        }

        public Task<Unit> Handle(AddChatCmdCommand request, CancellationToken cancellationToken) {
            _appSettings.ChatCommands.Commands.Add(new ChatCommand(_keybindProvider) {
                Key = request.Key,
                Command = request.Command,
                Send = request.Send,
                Enabled = true
            });

            return Unit.Task;
        }
    }
}
