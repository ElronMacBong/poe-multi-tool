﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

using Newtonsoft.Json;

using PMT.Domain.Settings;

namespace PMT.Application.Settings {
    public class SettingsReader : ISettingsReader {
        public string ConfigurationFilePath { get; set; }

        private ILogger _logger;

        private static readonly JsonSerializerSettings JsonSerializerSettings = new JsonSerializerSettings() {
            ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        };

        public SettingsReader(
            ILogger<SettingsReader> logger) {
            _logger = logger;
        }

        public T Load<T>() where T : class, new() {
            return Load(typeof(T)) as T;
        }
        public object Load(Type type) {
            if (!File.Exists(ConfigurationFilePath))
                return Activator.CreateInstance(type);

            return JsonConvert.DeserializeObject(File.ReadAllText(ConfigurationFilePath), type, JsonSerializerSettings);
        }



        public T LoadSection<T>() where T : class, new() {
            return LoadSection(typeof(T)) as T;
        }

        public object LoadSection(Type type) {
            if (!File.Exists(ConfigurationFilePath))
                return Activator.CreateInstance(type);

            string json = File.ReadAllText(ConfigurationFilePath);
            dynamic data = JsonConvert.DeserializeObject<dynamic>(json, JsonSerializerSettings);
            dynamic section = data[type.Name.Replace("Settings", string.Empty)];

            return section == null
                ? Activator.CreateInstance(type)
                : JsonConvert.DeserializeObject(JsonConvert.SerializeObject(section), type, JsonSerializerSettings);
        }
    }
}
