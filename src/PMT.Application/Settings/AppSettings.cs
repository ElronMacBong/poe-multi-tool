﻿using System;
using System.Collections.Generic;
using System.Text;

using PMT.Domain.Game.ChatCommand;
using PMT.Domain.Settings;

namespace PMT.Application.Settings {
    public class AppSettings : IAppSettings {
        public ISettingsSettings Settings { get; set; }
        public IGeneralSettings General { get; set; }
        public IPathOfExileSettings PathOfExile { get; set; }
        public IOverlaySettings Overlay { get; set; }
        public IChatCommandsSettings ChatCommands { get; set; }
        public IGuideSettings Guide { get; set; }
        public ITradeSettings Trade { get; set; }

        public AppSettings(
            ISettingsReader settingsReader) {
            General = settingsReader.LoadSection<GeneralSettings>();

            PathOfExile = settingsReader.LoadSection<PathOfExileSettings>();

            Overlay = settingsReader.LoadSection<OverlaySettings>();

            ChatCommands = settingsReader.LoadSection<ChatCommandsSettings>();

            Guide = settingsReader.LoadSection<GuideSettings>();

            Trade = settingsReader.LoadSection<TradeSettings>();
        }
    }
}
