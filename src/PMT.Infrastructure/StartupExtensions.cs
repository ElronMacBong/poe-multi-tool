﻿using System;

using Microsoft.Extensions.DependencyInjection;

using PMT.Infrastructure.PoeApi;

namespace PMT.Infrastructure {
    public static class StartupExtensions {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services) {
            services.AddTransient<IPoeClient, PoeClient>();

            return services;
        }
    }
}
