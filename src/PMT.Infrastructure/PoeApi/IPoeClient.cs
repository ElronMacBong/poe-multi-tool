﻿using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace PMT.Infrastructure.PoeApi {
    public interface IPoeClient {
        HttpClient HttpClient { get; set; }
        JsonSerializerOptions Options { get; }

        Task<FetchResult<TReturn>> Fetch<TReturn>(string path, bool useDefaultLanguage = false);
    }
}
