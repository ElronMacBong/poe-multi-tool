﻿using System.Collections.Generic;

namespace PMT.Infrastructure.PoeApi {
    public class FetchResult<T> {
        public List<T> Result { get; set; }
    }
}
