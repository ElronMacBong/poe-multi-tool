﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

using MediatR;

using Microsoft.Extensions.Logging;

using PMT.Domain.Game.Languages;

namespace PMT.Infrastructure.PoeApi {
    public class PoeClient : IPoeClient {
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly IGameLanguageProvider _gameLanguageProvider;

        public HttpClient HttpClient { get; set; }
        public JsonSerializerOptions Options { get; }

        public PoeClient(
            ILogger<PoeClient> logger,
            IMediator mediator,
            IGameLanguageProvider gameLanguageProvider,
            IHttpClientFactory httpClientFactory) {
            _logger = logger;
            _mediator = mediator;
            _gameLanguageProvider = gameLanguageProvider;

            HttpClient = httpClientFactory.CreateClient();
            HttpClient.DefaultRequestHeaders.TryAddWithoutValidation("X-Powered-By", Assembly.GetExecutingAssembly().GetName().Name);
            HttpClient.DefaultRequestHeaders.UserAgent.TryParseAdd(Assembly.GetExecutingAssembly().GetName().Name);

            Options = new JsonSerializerOptions() {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                IgnoreNullValues = true
            };
            Options.Converters.Add(new JsonStringEnumConverter(JsonNamingPolicy.CamelCase));
        }

        public async Task<FetchResult<TReturn>> Fetch<TReturn>(string path, bool useDefaultLanguage = false) {
            string name = typeof(TReturn).Name;
            IGameLanguage language = _gameLanguageProvider.Language;

            /*
            if (useDefaultLanguage || language == null)
                language = await _mediator.Send(new GetGameLanguageCommand("en-US"));
            */

            try {
                HttpResponseMessage response = await HttpClient.GetAsync(language.PoeApiBaseUrl + path);
                Stream content = await response.Content.ReadAsStreamAsync();

                return await JsonSerializer.DeserializeAsync<FetchResult<TReturn>>(content, Options);
            } catch (Exception) {
                _logger.LogWarning($"Could not fetch {name} at {language.PoeApiBaseUrl + path}");
                throw;
            }
        }
    }
}
