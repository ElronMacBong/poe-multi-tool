﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using PMT.Domain.Platforms;

namespace PMT.Platform.Clipboard {
    public class ClipboardProvider : IClipboardProvider {
        private readonly IKeyboardProvider _keyboardProvider;

        public ClipboardProvider(
            IKeyboardProvider keyboardProvider) {
            _keyboardProvider = keyboardProvider;
        }

        public async Task<string> Copy() {
            string clipboardText = string.Empty;

            await SetText(string.Empty);
            _keyboardProvider.PressKey("Copy");
            await Task.Delay(100);

            string result = await TextCopy.ClipboardService.GetTextAsync();

            return result;
        }

        public async Task<string> GetText() {
            return await TextCopy.ClipboardService.GetTextAsync();
        }

        public async Task SetText(string text) {
            if (text == null)
                text = string.Empty;

            await TextCopy.ClipboardService.SetTextAsync(text);
        }
    }
}
