﻿using System;
using System.Runtime.InteropServices;

using Microsoft.Extensions.DependencyInjection;

using PMT.Domain.Platforms;
using PMT.Platform.Clipboard;
using PMT.Platform.Windows.Keyboard;
using PMT.Platform.Windows.Processes;

namespace PMT.Platform {
    public static class StartupExtensions {
        public static IServiceCollection AddPlatform(this IServiceCollection services) {
            services.AddTransient<IClipboardProvider, ClipboardProvider>();

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) {
                services.AddSingleton<IProcessProvider, ProcessProvider>();
                services.AddSingleton<IKeyboardProvider, KeyboardProvider>();
            }

            return services;
        }
    }
}
