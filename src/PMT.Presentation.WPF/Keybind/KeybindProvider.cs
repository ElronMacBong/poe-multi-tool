﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MediatR;

using Microsoft.Extensions.Logging;

using PMT.Domain.App.Commands;
using PMT.Domain.Game.ChatCommand;
using PMT.Domain.Keybinds;
using PMT.Domain.Platforms;
using PMT.Domain.Settings;

namespace PMT.Presentation.WPF.Keybind {
    public class KeybindProvider : IKeybindProvider, IDisposable {
        private readonly ILogger _logger;
        private readonly IAppSettings _appSettings;
        private readonly IProcessProvider _processProvider;
        private readonly IKeyboardProvider _keyboardProvider;

        private List<IKeybind> _keybinds = new List<IKeybind>();

        public KeybindProvider(
            ILogger<KeybindProvider> logger,
            IAppSettings appSettings,
            IProcessProvider processProvider,
            IKeyboardProvider keyboardProvider) {
            _logger = logger;
            _appSettings = appSettings;
            _processProvider = processProvider;
            _keyboardProvider = keyboardProvider;
        }

        public void Initialize() {
            _processProvider.OnFocus += Register;
            _processProvider.OnBlur += Unregister;

            foreach (IChatCommand setting in _appSettings.ChatCommands.Commands) {
                AddChatCommand(setting.Key, setting.Command, setting.Send, setting.Enabled);
            }
        }

        public void Dispose() {
            _processProvider.OnFocus -= Register;
            _processProvider.OnBlur -= Unregister;
        }



        public void Register() {
            _keyboardProvider.OnKeyDown += OnKeyboardDownEvent;
        }

        public void Unregister() {
            _keyboardProvider.OnKeyDown -= OnKeyboardDownEvent;
        }



        public void AddChatCommand(string key, string command, bool send, bool enabled) {
            ChatCommandKeybind chatCommand = App.GetService<ChatCommandKeybind>();
            chatCommand.Command.Key = key;
            chatCommand.Command.Command = command;
            chatCommand.Command.Send = send;
            chatCommand.Command.Enabled = enabled;

            _keybinds.Add(chatCommand);
        }

        public void RemoveChatCommand(string key) {
            _logger.LogDebug($"RemoveChatCommand({key})");


        }



        private void OnKeyboardDownEvent(string key) {
            _logger.LogDebug($"OnKeyboardDownEvent({key})");

            IKeybind keybind = GetKeybind(key);
            if (keybind == null)
                return;

            _logger.LogDebug($"Found keybind: {keybind.Command.Key}");
            keybind.Execute();
        }

        private IKeybind GetKeybind(string keybind) {
            return _keybinds.FirstOrDefault(x => x.Command.Key == keybind);
        }
    }
}
