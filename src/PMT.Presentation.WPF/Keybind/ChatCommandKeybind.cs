﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Extensions.Logging;

using PMT.Domain.Game.ChatCommand;
using PMT.Domain.Keybinds;
using PMT.Domain.Platforms;

namespace PMT.Presentation.WPF.Keybind {
    public class ChatCommandKeybind : IKeybind {
        private ILogger _logger;
        private IKeyboardProvider _keyboardProvider;
        private IClipboardProvider _clipboardProvider;

        public IChatCommand Command { get; set; }

        public ChatCommandKeybind(
            ILogger<ChatCommandKeybind> logger,
            IKeyboardProvider keyboardProvider,
            IClipboardProvider clipboardProvider,
            IKeybindProvider keybindProvider) {
            _logger = logger;
            _keyboardProvider = keyboardProvider;
            _clipboardProvider = clipboardProvider;

            Command = new ChatCommand(keybindProvider);
        }

        public void Execute() {
            if (Command.Enabled == false)
                return;

            _clipboardProvider.SetText(string.Empty);
            _clipboardProvider.SetText(Command.Command);
            if (Command.Send)
                _keyboardProvider.PressKey("Enter", "Paste", "Enter");
            else
                _keyboardProvider.PressKey("Enter", "Paste");

            _clipboardProvider.SetText(string.Empty);
        }
    }
}
