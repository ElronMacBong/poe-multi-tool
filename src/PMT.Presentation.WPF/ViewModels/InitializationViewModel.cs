﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

using PMT.Domain.ViewModels;

namespace PMT.Presentation.WPF.ViewModels {
    public class InitializationViewModel : IInitializationViewModel, INotifyPropertyChanged {
        private int _progress = 0;
        public int Progress {
            get => _progress;
            set {
                // Clamp value between 0 and 100
                if (value < 0)
                    value = 0;
                if (value > 100)
                    value = 100;

                _progress = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
