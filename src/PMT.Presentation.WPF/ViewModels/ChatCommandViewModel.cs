﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

using Microsoft.Extensions.Logging;

using PMT.Domain.Game.ChatCommand;
using PMT.Domain.ViewModels;

namespace PMT.Presentation.WPF.ViewModels {
    public class ChatCommandViewModel : IChatCommandViewModel, INotifyPropertyChanged {
        private ILogger _logger;

        private IChatCommand _command;

        public IChatCommand Command {
            get => _command;
            set {
                _command = value;
                OnPropertyChanged();
            }
        }

        public ChatCommandViewModel() {
            _logger = App.GetService<ILogger<ChatCommandViewModel>>();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
