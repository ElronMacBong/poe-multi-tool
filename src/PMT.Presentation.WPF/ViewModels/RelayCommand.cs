﻿using System;
using System.Windows.Input;

namespace PMT.Presentation.WPF.ViewModels {
    public class RelayCommand : ICommand {
        private readonly Action<object> _action;

        public RelayCommand(Action<object> action) {
            _action = action;
        }

        #region ICommand Members

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter) {
            return true;
        }

        public void Execute(object parameter) {
            _action(parameter);
        }

        #endregion
    }
}
