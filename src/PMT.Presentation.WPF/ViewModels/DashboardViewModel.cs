﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using PMT.Application.Settings;
using PMT.Domain.Platforms;
using PMT.Domain.Settings;
using PMT.Domain.Views;
using PMT.Presentation.WPF.Views;

namespace PMT.Presentation.WPF.ViewModels {
    public class DashboardViewModel : IDashboardViewModel, INotifyPropertyChanged {
        public ICommand QuitCommand {
            get => _quitCommand;
            set => _quitCommand = value;
        }
        public ICommand SettingsCommand {
            get => _settingsCommand;
            set => _settingsCommand = value;
        }
        public ICommand ChangelogCommand {
            get => _changelogCommand;
            set => _changelogCommand = value;
        }
        public ICommand AboutCommand {
            get => _aboutCommand;
            set => _aboutCommand = value;
        }
        public ICommand SaveConfigCommand {
            get => _saveConfigCommand;
            set => _saveConfigCommand = value;
        }

        private AppSettings _appSettings;
        public AppSettings AppSettings {
            get => _appSettings;
            set {
                _appSettings = value;
                OnPropertyChanged();
            }
        }

        public string PoeRunningColor {
            get => _poeRunningColor;
            set {
                _poeRunningColor = value;
                OnPropertyChanged();
            }
        }
        public string PoeFocusColor {
            get => _poeFocusColor;
            set {
                _poeFocusColor = value;
                OnPropertyChanged();
            }
        }
        public string IsPoeRunning {
            get => _isPoeRunning;
            set {
                _isPoeRunning = value;
                OnPropertyChanged();
            }
        }
        public string IsPoeInFocus {
            get => _isPoeInFocus;
            set {
                _isPoeInFocus = value;
                OnPropertyChanged();
            }
        }

        private ICommand _quitCommand;
        private ICommand _settingsCommand;
        private ICommand _changelogCommand;
        private ICommand _aboutCommand;
        private ICommand _saveConfigCommand;

        private string _poeRunningColor = "black";
        private string _poeFocusColor = "black";
        private string _isPoeRunning = "n/a";
        private string _isPoeInFocus = "n/a";



        public DashboardViewModel() {
            QuitCommand = new RelayCommand(new Action<object>(OnQuitClicked));
            SettingsCommand = new RelayCommand(new Action<object>(OnSettingsClicked));
            ChangelogCommand = new RelayCommand(new Action<object>(OnChangelogClicked));
            AboutCommand = new RelayCommand(new Action<object>(OnAboutClicked));
            SaveConfigCommand = new RelayCommand(new Action<object>(OnSaveConfigClicked));

            AppSettings = (AppSettings)App.GetService<IAppSettings>();

            Task.Run(UpdateProcessStatus);
        }



        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }



        public Task UpdateProcessStatus() {
            while (true) {
                if (App.GetService<IProcessProvider>().GetPathOfExileProcess() != null) {
                    IsPoeRunning = "Running";
                    PoeRunningColor = "green";

                    if (App.GetService<IProcessProvider>().IsPathOfExileInFocus) {
                        IsPoeInFocus = "(focused)";
                        PoeFocusColor = "green";
                    } else {
                        IsPoeInFocus = "(not focused)";
                        PoeFocusColor = "red";
                    }
                } else {
                    IsPoeRunning = "Not running";
                    PoeRunningColor = "red";
                    IsPoeInFocus = "(n/a)";
                }
            }
        }



        public void OnQuitClicked(object obj) {
            App.GetService<Mediator.Mediator>().Send(new Domain.App.Commands.ShutdownCommand());
        }

        public void OnSettingsClicked(object obj) {
            ((ViewHandler)App.GetService<IViewHandler>()).OpenModal(View.Settings, App.GetService<DashboardView>());
        }

        public void OnChangelogClicked(object obj) { }

        public void OnAboutClicked(object obj) {
            //App.GetService<ViewHandler>().OpenModal(View.About);

            /*
            AboutView window = App.GetService<AboutView>();
            window.Owner = App.GetService<DashboardView>();
            window.Show();
            */
        }

        public void OnSaveConfigClicked(object obj) {
            App.GetService<ISettingsWriter>().WriteAsync<IAppSettings>(App.GetService<IAppSettings>());
        }
    }
}
