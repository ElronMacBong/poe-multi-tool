﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;

using Microsoft.Extensions.Logging;

using PMT.Application.Settings;
using PMT.Domain.Settings;
using PMT.Domain.ViewModels;
using PMT.Domain.Views;
using PMT.Presentation.WPF.Views;

namespace PMT.Presentation.WPF.ViewModels {
    public class SettingsViewModel : ISettingsViewModel, INotifyPropertyChanged {
        private ILogger _logger;

        public ICommand AddNewChatCommand { get; set; }

        private AppSettings _appSettings;
        public AppSettings AppSettings {
            get => _appSettings;
            set {
                _appSettings = value;
                OnPropertyChanged();
            }
        }

        public SettingsViewModel() {
            _logger = App.GetService<ILogger<SettingsViewModel>>();
            AppSettings = (AppSettings)App.GetService<IAppSettings>();

            AddNewChatCommand = new RelayCommand(new Action<object>(OnAddNewCommandClicked));
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }



        public void OnAddNewCommandClicked(object obj) {
            ((ViewHandler)App.GetService<IViewHandler>()).OpenModal(View.AddChatCommand, App.GetService<SettingsView>());
        }
    }
}
