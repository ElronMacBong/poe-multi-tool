﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

using Microsoft.Extensions.Logging;

using PMT.Domain.App.Commands;
using PMT.Domain.ViewModels;
using PMT.Domain.Views;

namespace PMT.Presentation.WPF.ViewModels {
    public class AddChatCommandsViewModel : IAddChatCommandsViewModel {
        private ILogger _logger;

        public ICommand AddChatCommand { get; set; }

        public string Key { get; set; }
        public string Command { get; set; }
        public bool Send { get; set; }

        public AddChatCommandsViewModel() {
            _logger = App.GetService<ILogger<AddChatCommandsViewModel>>();
            AddChatCommand = new RelayCommand(new Action<object>(OnAddChatCommandClicked));
        }

        public void OnAddChatCommandClicked(object obj) {
            //App.GetService<Mediator.Mediator>().Send(new AddChatCmdCommand(Key, Command, Send));

            App.GetService<IViewHandler>().Close(View.AddChatCommand);
        }
    }
}
