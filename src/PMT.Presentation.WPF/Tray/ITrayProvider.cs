﻿using System.Threading.Tasks;

namespace PMT.Presentation.WPF.Tray {
    internal interface ITrayProvider {
        bool IsInitialized { get; }

        Task Initialize();
        Task Dispose();
    }
}