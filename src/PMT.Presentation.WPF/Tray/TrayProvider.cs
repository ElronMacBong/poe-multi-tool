﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MediatR;

using Microsoft.Extensions.Logging;

using PMT.Domain.App.Commands;
using PMT.Domain.Views;

namespace PMT.Presentation.WPF.Tray {
    public class TrayProvider : ITrayProvider {
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly IViewHandler _viewHandler;
        private readonly NotifyIcon _trayIcon;

        private bool _isInitialized = false;
        public bool IsInitialized => _isInitialized;

        public TrayProvider(
            ILogger<TrayProvider> logger,
            IMediator mediator,
            IViewHandler viewHandler
            ) {
            _logger = logger;
            _mediator = mediator;
            _viewHandler = viewHandler;
            _trayIcon = new NotifyIcon();
        }

        public Task Initialize() {
            _trayIcon.Icon = new Icon("Resources/icon.ico");
            _trayIcon.Text = "PoE Multi-Tool";

            _trayIcon.ContextMenuStrip = new ContextMenuStrip();
            _trayIcon.ContextMenuStrip.Items.Add("Show PoE Multi-Tool", null, OnShowPMTClicked);
            _trayIcon.ContextMenuStrip.Items.Add(new ToolStripSeparator());
#if DEBUG
            _trayIcon.ContextMenuStrip.Items.Add("Show Overlay", null, OnOverlayClicked);
            _trayIcon.ContextMenuStrip.Items.Add("Show Browser", null, OnWebBrowserClicked);
            _trayIcon.ContextMenuStrip.Items.Add(new ToolStripSeparator());
#endif
            _trayIcon.ContextMenuStrip.Items.Add("Quit", null, OnQuitClicked);

            _trayIcon.DoubleClick += OnDoubleClick;

            _trayIcon.Visible = true;

            _isInitialized = true;
            return Task.CompletedTask;
        }

        public Task Dispose() {
            _trayIcon.Dispose();

            _isInitialized = false;
            return Task.CompletedTask;
        }



        private void OnDoubleClick(object sender, EventArgs e) {
            _viewHandler.Open(Domain.Views.View.Dashboard);
        }

        private void OnShowPMTClicked(object sender, EventArgs e) {
            _viewHandler.Open(Domain.Views.View.Dashboard);
        }

        private void OnOverlayClicked(object sender, EventArgs e) {
            _viewHandler.Open(Domain.Views.View.MainOverlay);
        }

        private void OnWebBrowserClicked(object sender, EventArgs e) {
            _viewHandler.Open(Domain.Views.View.WebBrowser);
        }

        private void OnQuitClicked(object sender, EventArgs e) {
            _mediator.Send(new ShutdownCommand());
        }
    }
}
