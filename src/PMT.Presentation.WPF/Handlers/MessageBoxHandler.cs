﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

using MediatR;

using PMT.Domain.MessageBox.Commands;

namespace PMT.Presentation.WPF.Handlers {
    public class MessageBoxHandler : ICommandHandler<MessageBoxCommand> {
        public Task<Unit> Handle(MessageBoxCommand request, CancellationToken cancellationToken) {
            MessageBox.Show(request.Text, request.Caption, (System.Windows.MessageBoxButton)request.Button, (System.Windows.MessageBoxImage)request.Icon, (System.Windows.MessageBoxResult)request.Result, (System.Windows.MessageBoxOptions)request.Options);

            return Unit.Task;
        }
    }
}
