﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using PMT.Domain.Notification.Commands;

namespace PMT.Presentation.WPF.Handlers {
    public class NotificationHandler : ICommandHandler<NotificationCommand> {
        private IMediator _mediator;

        public NotificationHandler(IMediator mediator) {
            _mediator = mediator;
        }

        public Task<Unit> Handle(NotificationCommand request, CancellationToken cancellationToken) {


            return Unit.Task;
        }
    }
}
