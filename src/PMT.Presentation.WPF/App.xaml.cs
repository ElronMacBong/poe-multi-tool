﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using PMT.Application;
using PMT.Domain.App.Commands;
using PMT.Domain.Game.ChatCommand;
using PMT.Domain.Initialization.Commands;
using PMT.Domain.Platforms;
using PMT.Domain.Settings;
using PMT.Domain.ViewModels;
using PMT.Domain.Views;
using PMT.Logging;
using PMT.Mediator;
using PMT.Platform;
using PMT.Presentation.WPF.Keybind;
using PMT.Presentation.WPF.Tray;
using PMT.Presentation.WPF.ViewModels;
using PMT.Presentation.WPF.Views;

using MessageBox = System.Windows.MessageBox;

namespace PMT.Presentation.WPF {
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : System.Windows.Application {
        private readonly string _appDataPath;
        private readonly string _appDataCommonPath;
        private readonly string _appDataLocalPath;
        private readonly string _logFile;
        private readonly string _configFile;

        private readonly IConfiguration _configuration;
        private static ServiceProvider ServiceProvider;

        private readonly ILogger _logger;

        public static T GetService<T>() {
            return ServiceProvider.GetService<T>();
        }



        public App() {
            // Setup paths
            _appDataPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), Assembly.GetExecutingAssembly().GetName().Name);
            _appDataCommonPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), Assembly.GetExecutingAssembly().GetName().Name);
            _appDataLocalPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), Assembly.GetExecutingAssembly().GetName().Name);
            _logFile = Path.Combine(_appDataPath, "logs", "");
            _configFile = Path.Combine(_appDataPath, "config.json");

            // Add configuration
            _configuration = AddConfiguration();

            // Initialize application
            try {
                ServiceCollection services = new ServiceCollection();
                ConfigureServices(services);
                ServiceProvider = services.BuildServiceProvider();

                _logger = GetService<ILogger<App>>();

                _logger.LogInformation($"{Assembly.GetExecutingAssembly().GetName().Name} v{Assembly.GetExecutingAssembly().GetName().Version} initialized.");
            } catch (Exception ex) {
                MessageBox.Show($"Failed to initialize application!\n{ex.Message}", "Initialization failed", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        protected override async void OnStartup(StartupEventArgs e) {
            base.OnStartup(e);

            _logger.LogInformation("Starting PoE Multi-Tool...");
            try {
                _logger.LogInformation($"Set configuration path to '{_configFile}'.");
                GetService<ISettingsReader>().ConfigurationFilePath = _configFile;
                GetService<ISettingsWriter>().ConfigurationFilePath = _configFile;

                _logger.LogInformation($"Initialize tray provider");
                await GetService<ITrayProvider>().Initialize();

                _logger.LogDebug($"Sending initialize command");
                await GetService<Mediator.Mediator>().Send(new InitializeCommand(true, true));
            } catch (Exception ex) {
                _logger.LogError($"Could not initialize PoE Multi-Tool!\n{ex.Message}");
                await GetService<Mediator.Mediator>().Send(new ShutdownCommand());
            }
        }

        protected override void OnActivated(EventArgs e) { base.OnActivated(e); }

        protected override void OnDeactivated(EventArgs e) { base.OnDeactivated(e); }

        protected override async void OnExit(ExitEventArgs e) {
            _logger.LogInformation("Received exit event. Starting cleanup...");

            _logger.LogInformation("Save settings");
            await GetService<ISettingsWriter>().WriteAsync<IAppSettings>(GetService<IAppSettings>());

            _logger.LogInformation("Dispose tray provider");
            await GetService<ITrayProvider>().Dispose();

            _logger.LogInformation("Exiting PoE Multi-Tool...\n");
            Serilog.Log.CloseAndFlush();

            base.OnExit(e);
        }

        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e) {
            _logger.LogError($"An unhandled exception just occured: {e.Exception.Message}");
            MessageBox.Show($"An unhandled exception just occured: {e.Exception.Message}", "Unhandled exception", MessageBoxButton.OK, MessageBoxImage.Warning);

            e.Handled = true;
        }



        private IConfiguration AddConfiguration() {
            IConfigurationBuilder builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
#if DEBUG
            builder.AddJsonFile("appsettings.Development.json", optional: true, reloadOnChange: true);
#else
            builder.AddJsonFile("appsettings.Production.json", optional: true, reloadOnChange: true);
#endif

            return builder.Build();
        }

        private void ConfigureServices(ServiceCollection services) {
            // Add libraries
            services
                .AddApplication(_configuration)
                .AddPmtLogging(_logFile)
                .AddPlatform();

            // Mediator
            services.AddMediator(
                Assembly.Load("PMT.Application"),
                Assembly.Load("PMT.Domain"),
                Assembly.Load(Assembly.GetEntryAssembly().GetName()));

            // Views
            GetType().Assembly.GetTypes()
                .Where(type => type.Name.EndsWith("View"))
                .ToList()
                .ForEach(viewType => services.AddSingleton(viewType, viewType));

            // User Controls
            GetType().Assembly.GetTypes()
                .Where(type => type.Name.EndsWith("UserControl"))
                .ToList()
                .ForEach(controlType => services.AddSingleton(controlType, controlType));

            // View Models
            services
                .AddSingleton<IInitializationViewModel, InitializationViewModel>()
                .AddSingleton<ISetupViewModel, SetupViewModel>()
                .AddSingleton<IDashboardViewModel, DashboardViewModel>()
                .AddSingleton<ISettingsViewModel, SettingsViewModel>()
                .AddSingleton<IMainOverlayViewModel, MainOverlayViewModel>()
                .AddScoped<IAddChatCommandsViewModel, AddChatCommandsViewModel>();

            // Misc
            services
                .AddSingleton<IKeybindProvider, KeybindProvider>()
                .AddSingleton<ITrayProvider, TrayProvider>()
                .AddSingleton<IViewHandler, ViewHandler>()
                .AddScoped<IViewInstance, ViewInstance>();

            services
                .AddTransient<ChatCommandKeybind>();
        }



        /*
        private void ConfigureServices(ServiceCollection services) {
            services
                .AddApplication(_configuration)
                .AddInfrastructure()
                .AddLocalization()
                .AddLogging()
                .AddPlatform();

            services
                .AddMediator(
                    Assembly.Load("PMT.Application"),
                    Assembly.Load("PMT.Domain"),
                    Assembly.Load("PMT.Infrastructure"),
                    Assembly.Load(Assembly.GetEntryAssembly().GetName()));

            services
                .AddSingleton<TrayProvider>()
                .AddSingleton<ViewHandler>()
                .AddSingleton<IViewHandler>(implementationFactory: (sp) => sp.GetRequiredService<ViewHandler>())
                .AddScoped<IViewInstance, ViewInstance>();

            GetType().Assembly.GetTypes()
                .Where(type => type.Name.EndsWith("View"))
                .ToList()
                .ForEach(viewType => services.AddSingleton(viewType, viewType));

            GetType().Assembly.GetTypes()
                .Where(type => type.Name.EndsWith("ViewModel"))
                .ToList()
                .ForEach(viewType => services.AddSingleton(viewType, viewType));
        }

        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e) {
            MessageBox.Show($"An unhandled exception just occured: {e.Exception.Message}", "Unhandled exception", MessageBoxButton.OK, MessageBoxImage.Warning);

            e.Handled = true;
        }
        */
    }
}
