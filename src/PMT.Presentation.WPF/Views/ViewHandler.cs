﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using MediatR;

using PMT.Domain.Views;

namespace PMT.Presentation.WPF.Views {
    public class ViewHandler : IViewHandler, IDisposable {
        internal List<ViewInstance> Views { get; set; } = new List<ViewInstance>();

        public ViewHandler() { }

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing) {
            CloseAll();
        }



        public Task Open(View view, params object[] args) {
            if (IsOpened(view))
                Close(view);

            Views.Add(new ViewInstance(this, view));

            return Task.CompletedTask;
        }
        public Task OpenModal(View view, Window owner) {
            if (IsOpened(view))
                Close(view);

            Views.Add(new ViewInstance(this, view, true, owner));

            return Task.CompletedTask;
        }

        public void Close(View view) {
            Views.Where(x => x.View == view).FirstOrDefault()?.Close();
            Views.RemoveAll(x => x.View == view);
        }
        public void CloseAll() {
            foreach (ViewInstance viewInstance in Views) {
                viewInstance.Close();
            }

            Views.Clear();
        }

        public bool IsAnyOpened() {
            return Views.Any();
        }

        public bool IsOpened(View view) {
            return Views.Where(x => x.View == view) != null;
        }
    }
}
