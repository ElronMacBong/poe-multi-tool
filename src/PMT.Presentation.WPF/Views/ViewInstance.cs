﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;

using Microsoft.Extensions.Logging;

using PMT.Domain.Views;

namespace PMT.Presentation.WPF.Views {
    public class ViewInstance : IViewInstance {
        private readonly ILogger _logger;

        private readonly IViewHandler _viewHandler;
        private readonly Window _window;

        public View View { get; }
        public ViewSettings Settings { get; } = new ViewSettings();



        public ViewInstance(IViewHandler viewHandler, View view) : this(viewHandler, view, false) { }
        public ViewInstance(IViewHandler viewHandler, View view, bool modal, Window parentView = null) {
            _viewHandler = viewHandler;
            View = view;

            Settings.IsModal = modal;

            _window = GetWindow(view);
            if (_window != null) {
                _window.Closing += OnClosing;

                if (Settings.IsModal) {
                    Settings.ShowInTaskbar = false;

                    _window.ShowInTaskbar = Settings.ShowInTaskbar;
                    _window.Owner = parentView;
                }

                _window.Show();
            } else {
                MessageBox.Show($"Can't open view {view}!");
            }
        }

        internal Window GetWindow(View view) {
            Window window = null;
            switch (view) {
                case View.Initialization: window = App.GetService<InitializationView>(); break;
                case View.Setup: window = App.GetService<SetupView>(); break;
                case View.Dashboard: window = App.GetService<DashboardView>(); break;
                case View.Settings: window = App.GetService<SettingsView>(); break;
                case View.MainOverlay: window = App.GetService<MainOverlayView>(); break;
                case View.WebBrowser: window = App.GetService<WebBrowserView>(); break;

                case View.AddChatCommand: window = App.GetService<AddChatCommandView>(); break;
            };

            return window;
        }

        private void OnClosing(object sender, System.ComponentModel.CancelEventArgs e) {
            e.Cancel = true;

            Close();
        }



        public Task Close() {
            if (_window != null)
                _window.Hide();

            return Task.CompletedTask;
        }

        public Task Maximize() {
            throw new System.NotImplementedException();
        }

        public Task Minimize() {
            throw new System.NotImplementedException();
        }
    }
}
