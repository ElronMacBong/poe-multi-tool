﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using PMT.Domain.App.Commands;
using PMT.Presentation.WPF.ViewModels;

namespace PMT.Presentation.WPF.Views {
    /// <summary>
    /// Interaktionslogik für DashboardView.xaml
    /// </summary>
    public partial class DashboardView : Window {
        public DashboardView() {
            InitializeComponent();
        }
    }
}
