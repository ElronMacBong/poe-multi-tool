﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using PMT.Domain.App.Commands;

namespace PMT.Presentation.WPF {
    public class ShutdownHandler : ICommandHandler<ShutdownCommand> {
        public Task<Unit> Handle(ShutdownCommand request, CancellationToken cancellationToken) {
            System.Windows.Application.Current.Shutdown();

            return Unit.Task;
        }
    }
}
