﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using PMT.Domain.Initialization.Notifications;
using PMT.Domain.ViewModels;
using PMT.Presentation.WPF.ViewModels;

namespace PMT.Presentation.WPF.Initialization {
    public class InitializationHandler : INotificationHandler<InitializationProgressed> {
        public int Completed { get; }

        public Task Handle(InitializationProgressed notification, CancellationToken cancellationToken) {
            IInitializationViewModel vm = App.GetService<IInitializationViewModel>();
            vm.Progress = notification.Percentage;

            //InitializationViewModel model = App.GetService<InitializationViewModel>();
            //model.Progress = notification.Percentage;

            return Task.CompletedTask;
        }
    }
}
