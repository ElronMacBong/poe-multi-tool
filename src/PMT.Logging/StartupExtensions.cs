﻿using System.IO;

using Microsoft.Extensions.DependencyInjection;

using Serilog;

namespace PMT.Logging {
    public static class StartupExtensions {
        public static IServiceCollection AddPmtLogging(this IServiceCollection services, string logPath) {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.Debug()
                .WriteTo.File(Path.Combine(logPath, "pmt-.log"), rollingInterval: RollingInterval.Day)
                .CreateLogger();

            services
                .AddLogging(builder => {
                    builder.AddSerilog();
                });

            return services;
        }
    }
}
