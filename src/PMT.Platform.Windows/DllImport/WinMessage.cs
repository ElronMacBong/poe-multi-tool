﻿using System.Runtime.InteropServices;
using System;

namespace PMT.Platform.Windows.DllImport {
    [StructLayout(LayoutKind.Sequential)]
    internal struct WinMessage {
        public IntPtr Hwnd;
        public uint Message;
        public IntPtr WParam;
        public IntPtr LParam;
        public uint Time;
        public System.Drawing.Point Point;
    }
}