﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PMT.Platform.Windows.DllImport {
    internal static class EventLoop {
        public static CancellationTokenSource Run(uint eventMin, uint eventMax, IntPtr hmodWinEventProc, User32.WinEventDelegate eventDelegate, uint idProcess, uint idThread, uint dwFlags) {
            const uint PM_REMOVE = 1;
            const uint WM_QUIT = 0x0012;

            var cancellationToken = new CancellationTokenSource();

            Task.Run(() => {
                var handle = User32.SetWinEventHook(eventMin, eventMax, hmodWinEventProc, eventDelegate, idProcess, idThread, dwFlags);
                while (!cancellationToken.Token.IsCancellationRequested) {
                    if (User32.PeekMessage(out var msg, IntPtr.Zero, 0, 0, PM_REMOVE)) {
                        if (msg.Message == WM_QUIT)
                            break;

                        User32.TranslateMessage(ref msg);
                        User32.DispatchMessage(ref msg);
                    }

                    Thread.Sleep(200);
                }

                User32.UnhookWinEvent(handle);
            }, cancellationToken.Token);

            return cancellationToken;
        }

        internal static CancellationToken Run(object winEvent) {
            throw new NotImplementedException();
        }

        internal static CancellationToken Run(uint eVENT_SYSTEM_FOREGROUND, uint eVENT_SYSTEM_CAPTURESTART, IntPtr zero, object onWindowsEvent) {
            throw new NotImplementedException();
        }
    }
}
