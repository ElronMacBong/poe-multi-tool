﻿using System;
using System.Collections.Generic;
using System.Text;

using GregsStack.InputSimulatorStandard.Native;

using NeatInput.Windows.Processing.Keyboard.Enums;

namespace PMT.Platform.Windows.Keyboard {
    public struct Key {
        public Keys HookKey { get; }
        public VirtualKeyCode SendKey { get; }
        public string StringValue { get; }

        public Key(Keys hookKey, VirtualKeyCode sendKey, string stringValue) {
            HookKey = hookKey;
            SendKey = sendKey;
            StringValue = stringValue;
        }
    }
}
