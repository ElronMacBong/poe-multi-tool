﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;

using PMT.Domain.Platforms;
using PMT.Platform.Windows.DllImport;

namespace PMT.Platform.Windows.Processes {
    public class ProcessProvider : IProcessProvider, IDisposable {
        public event Action OnFocus;
        public event Action OnBlur;

        private const string PATH_OF_EXILE_TITLE = "Path of Exile";
        private const string POE_MULTI_TOOL_TITLE = "PoE Multi-Tool";
        private static readonly List<string> PossibleProcessNames = new List<string> { "PathOfExile", "PathOfExile_x64", "PathOfExileSteam", "PathOfExile_x64Steam" };

        private readonly ILogger _logger;
        private readonly IMediator _mediator;

        private string _focusedWindow;
        private bool _poeWasMinimized;
        private CancellationTokenSource _windowHook;

        public bool IsPathOfExileInFocus => _focusedWindow == PATH_OF_EXILE_TITLE;
        public bool IsPoeMultiToolInFocus => _focusedWindow == POE_MULTI_TOOL_TITLE;

        public ProcessProvider(
            ILogger<ProcessProvider> logger,
            IMediator mediator) {
            _logger = logger;
            _mediator = mediator;
        }

        public void Dispose() {
            _windowHook?.Cancel();
        }



        public Process GetPathOfExileProcess() {
            foreach (string processName in PossibleProcessNames) {
                Process process = Process.GetProcessesByName(processName).FirstOrDefault();

                if (process != null)
                    return process;
            }

            return null;
        }

        public Task Initialize(CancellationToken cancellationToken) {
            _windowHook = EventLoop.Run(WinEvent.EVENT_SYSTEM_FOREGROUND, WinEvent.EVENT_SYSTEM_CAPTURESTART, IntPtr.Zero, OnWindowsEvent, 0, 0, WinEvent.WINEVENT_OUTOFCONTEXT);

            return Task.CompletedTask;
        }



        private void OnWindowsEvent(IntPtr hWinEventHook, uint eventType, IntPtr hwnd, int idObject, int idChild, uint dwEventThread, uint dwmsEventTime) {
            if (eventType == WinEvent.EVENT_SYSTEM_MINIMIZEEND || eventType == WinEvent.EVENT_SYSTEM_FOREGROUND) {
                _focusedWindow = GetWindowTitle(hwnd);
                _logger.LogDebug($"Focused window: {_focusedWindow}");

                if (IsPathOfExileInFocus) {
                    _logger.LogDebug("Path of Exile focused.");

                    _poeWasMinimized = false;
                    OnFocus?.Invoke();
                } else if (!_poeWasMinimized) {
                    _logger.LogDebug("Path of Exile minimized.");

                    _poeWasMinimized = true;
                    OnBlur?.Invoke();
                }

                /*
                if (IsPathOfExileInFocus || IsPoeMultiToolInFocus) {
                    _logger.LogDebug("Path of Exile focused.");

                    _poeWasMinimized = false;
                    OnFocus?.Invoke();
                } else if (!_poeWasMinimized) {
                    _logger.LogDebug("Path of Exile minimized.");

                    _poeWasMinimized = true;
                    OnBlur?.Invoke();
                }
                */
            }
        }

        private static string GetWindowTitle(IntPtr handle) {
            StringBuilder buffer = new StringBuilder(User32.GetWindowTextLength(handle) + 1);
            if (User32.GetWindowText(handle, buffer, buffer.Capacity) > 0) {
                return buffer.ToString();
            }

            return null;
        }



        /*
        private void OnWindowsEvent(IntPtr hWinEventHook, uint eventType, IntPtr hwnd, int idObject, int idChild, uint dwEventThread, uint dwmsEventTime) {
            if (eventType == WinEvent.EVENT_SYSTEM_MINIMIZEEND || eventType == WinEvent.EVENT_SYSTEM_FOREGROUND) {
                _focusedWindow = GetWindowTitle(hwnd);

                if (IsPathOfExileInFocus || IsPMTInFocus) {
                    //_logger.LogInformation("Path of Exile focused.");
                    _poeWasMinimized = false;
                    OnFocus?.Invoke();
                } else if (!_poeWasMinimized) {
                    //_logger.LogInformation("Path of Exile minimized.");
                    _poeWasMinimized = true;
                    OnBlur?.Invoke();
                }

                // If the game is run as administrator, PMT also needs administrator privileges
                if (!_permissionChecked && IsPathOfExileInFocus) {
                    _permissionChecked = true;

                    _ = Task.Run(async () => {
                        if (!IsUserRunAsAdmin() && IsPathOfExileRunAsAdmin())
                            await RestartAsAdmin();
                    });
                }
            }
        }

        private string GetWindowTitle(IntPtr handle) {
            StringBuilder buffer = new StringBuilder(User32.GetWindowTextLength(handle) + 1);

            return User32.GetWindowText(handle, buffer, buffer.Capacity) > 0 ? buffer.ToString() : null;
        }

        private static Process GetPathOfExileProcess() {
            foreach (string processName in PossibleProcessNames) {
                Process process = Process.GetProcessesByName(processName).FirstOrDefault();
                if (process != null) return process;
            }

            return null;
        }
        */



        /*
        public event Action OnFocus;
        public event Action OnBlur;



        private const string POE_TITLE = "Path of Exile";
        private const string PMT_TITLE = "PoE Multi-Tool";
        private static readonly List<string> PossibleProcessNames = new List<string> { "PathOfExile", "PathOfExile_x64", "PathOfExileSteam", "PathOfExile_x64Steam" };

        private const int STANDARD_RIGHTS_REQUIRED = 0xF0000;
        private const int TOKEN_ASSIGN_PRIMARY = 0x1;
        private const int TOKEN_DUPLICATE = 0x2;
        private const int TOKEN_IMPERSONATE = 0x4;
        private const int TOKEN_QUERY = 0x8;
        private const int TOKEN_QUERY_SOURCE = 0x10;
        private const int TOKEN_ADJUST_GROUPS = 0x40;
        private const int TOKEN_ADJUST_PRIVILEGES = 0x20;
        private const int TOKEN_ADJUST_SESSIONID = 0x100;
        private const int TOKEN_ADJUST_DEFAULT = 0x80;
        private const int TOKEN_ALL_ACCESS = STANDARD_RIGHTS_REQUIRED | TOKEN_ASSIGN_PRIMARY | TOKEN_DUPLICATE | TOKEN_IMPERSONATE | TOKEN_QUERY | TOKEN_QUERY_SOURCE | TOKEN_ADJUST_PRIVILEGES | TOKEN_ADJUST_GROUPS | TOKEN_ADJUST_SESSIONID | TOKEN_ADJUST_DEFAULT;

        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly IStringLocalizer<ProcessProvider> _localizer;

        private CancellationTokenSource _windowsHook { get; set; }
        private string _focusedWindow { get; set; }
        private bool _permissionChecked { get; set; } = false;
        private bool _poeWasMinimized { get; set; }



        public string ClientLogPath => Path.Combine(Path.GetDirectoryName(GetPathOfExileProcess().GetMainModuleFileName()), "logs", "Client.txt");

        public bool IsPathOfExileInFocus => _focusedWindow == POE_TITLE;
        public bool IsPMTInFocus => _focusedWindow == PMT_TITLE;



        public ProcessProvider(
            IMediator mediator) {
            _mediator = mediator;
            //_localizer = localizer;
        }



        public Task Initialize(CancellationToken cancellationToken) {
            _windowsHook = EventLoop.Run(WinEvent.EVENT_SYSTEM_FOREGROUND, WinEvent.EVENT_SYSTEM_CAPTURESTART, IntPtr.Zero, OnWindowsEvent, 0, 0, WinEvent.WINEVENT_OUTOFCONTEXT);

            return Task.CompletedTask;
        }

        public void Dispose() {
            _windowsHook?.Cancel();
        }

        public Task DisposeAsync() {
            _windowsHook?.Cancel();

            return Task.CompletedTask;
        }



        private void OnWindowsEvent(IntPtr hWinEventHook, uint eventType, IntPtr hwnd, int idObject, int idChild, uint dwEventThread, uint dwmsEventTime) {
            if (eventType == WinEvent.EVENT_SYSTEM_MINIMIZEEND || eventType == WinEvent.EVENT_SYSTEM_FOREGROUND) {
                _focusedWindow = GetWindowTitle(hwnd);

                if (IsPathOfExileInFocus || IsPMTInFocus) {
                    //_logger.LogInformation("Path of Exile focused.");
                    _poeWasMinimized = false;
                    OnFocus?.Invoke();
                } else if (!_poeWasMinimized) {
                    //_logger.LogInformation("Path of Exile minimized.");
                    _poeWasMinimized = true;
                    OnBlur?.Invoke();
                }

                // If the game is run as administrator, PMT also needs administrator privileges
                if (!_permissionChecked && IsPathOfExileInFocus) {
                    _permissionChecked = true;

                    _ = Task.Run(async () => {
                        if (!IsUserRunAsAdmin() && IsPathOfExileRunAsAdmin())
                            await RestartAsAdmin();
                    });
                }
            }
        }

        private string GetWindowTitle(IntPtr handle) {
            StringBuilder buffer = new StringBuilder(User32.GetWindowTextLength(handle) + 1);

            return User32.GetWindowText(handle, buffer, buffer.Capacity) > 0 ? buffer.ToString() : null;
        }

        private static Process GetPathOfExileProcess() {
            foreach (string processName in PossibleProcessNames) {
                Process process = Process.GetProcessesByName(processName).FirstOrDefault();
                if (process != null) return process;
            }

            return null;
        }

        private bool IsPathOfExileRunAsAdmin() {
            bool result = false;

            try {
                User32.GetWindowThreadProcessId(User32.GetForegroundWindow(), out int processID);
                Process proc = Process.GetProcessById(processID);

                User32.OpenProcessToken(proc.Handle, TOKEN_ALL_ACCESS, out IntPtr ph);
                using (WindowsIdentity iden = new WindowsIdentity(ph)) {
                    foreach (IdentityReference role in iden.Groups) {
                        if (role.IsValidTargetType(typeof(SecurityIdentifier))) {
                            SecurityIdentifier sid = role as SecurityIdentifier;

                            if (sid.IsWellKnown(WellKnownSidType.AccountAdministratorSid) || sid.IsWellKnown(WellKnownSidType.BuiltinAdministratorsSid)) {
                                result = true;

                                break;
                            }
                        }
                    }

                    User32.CloseHandle(ph);
                }

                return result;
            } catch (Exception ex) {
                _logger.LogWarning(ex, ex.Message);
            }

            return result;
        }

        private bool IsUserRunAsAdmin() {
            WindowsIdentity info = WindowsIdentity.GetCurrent();
            WindowsPrincipal principle = new WindowsPrincipal(info);

            return principle.IsInRole(WindowsBuiltInRole.Administrator);
        }

        private async Task RestartAsAdmin() {
            //await _mediator.Send()
        }
        */
    }
}
