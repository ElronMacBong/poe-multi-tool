﻿using System;
using System.Collections.Generic;
using System.Text;

using Xunit;

namespace PMT.Application.Tests.Settings {
    public class SettingsHandlerTests {
        [Fact]
        public void CanReadSettingsValue() {
            // Arrange
            string expected = "TestValue";

            // Act
            string actual = "";

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
